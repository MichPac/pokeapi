import React,{useEffect,useState} from 'react'
import axios from 'axios'


const Pokemon = ({thisPokemon}) => {

    const[onePokemon,setOnePokemon] = useState({
        
        name: "",
        sprites: {
            front_default: ""
        },
        height:"",
        

    })

    const getPokemon = (url) => {
        axios.get(url)
            .then((response) => {
                console.log(response.data)
                setOnePokemon(response.data)
            })
            
    }

    useEffect(() => {
        getPokemon(thisPokemon.url)
    }, [])

    return (
        <div style={{backgroundColor:'lightblue',borderRadius:'30px',border:'1px solid',minWidth:'250px',margin:'50px auto'}}>
            <p>{onePokemon.name}</p>
            <img src={onePokemon.sprites.front_default}></img>
            <p>{onePokemon.height}</p>
            {onePokemon.types ?
                <div>{onePokemon.types.map(type => {
                    return <p
                            key={type.type.url}>
                            {type.type.name}</p>
                })}</div> : <p></p>
            }
            

        </div>
    )
}

export default Pokemon
