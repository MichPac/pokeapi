import React from 'react'
import Pokemon from './Pokemon'

const DisplayPokemon = ({pokemon})=> {

    return (
        <div>
            {pokemon ?
                pokemon.map((value,idx) => 
                    <div>
                    <Pokemon key={idx} thisPokemon={value} />
                    </div>
                ) : null
            }
        </div>
    )
}

export default DisplayPokemon
