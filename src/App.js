import './App.css';
import axios from 'axios'
import React,{useEffect, useState} from 'react'
import DisplayPokemon from './comp/DisplayPokemon';
import Pagination from './comp/Pagination';

function App() {

  const[pokemon,setPokemon] = useState([])
  const[currentPageUrl,setCurrentPageUrl] = useState('https://pokeapi.co/api/v2/pokemon')
  const[nextPageUrl,setNextPageUrl] = useState()
  const[prevPageUrl,setPrevPageUrl] = useState()
  const[loading,setLoading] = useState(true)

  const getPokemons = () =>{
    axios.get(currentPageUrl)
    .then((response) => {
      setLoading(false)
      setPokemon(response.data.results)
      setPrevPageUrl(response.data.previous)
      setNextPageUrl(response.data.next)
    })
  }
  
  useEffect(() => {
   getPokemons()
   
  }, [currentPageUrl])

  function gotoNextPage(){
    setLoading(true)
    setCurrentPageUrl(nextPageUrl)
  }

  function gotoPrevPage(){
    setLoading(true)
    setCurrentPageUrl(prevPageUrl)
    
  }

  return (
    <div className="App App-header">
    <div>
        <button onClick={gotoPrevPage}>Prev</button>
        <button onClick={gotoNextPage}>Next</button>
    </div>
    {loading ? <h1>fetching data...</h1> : 
    
      <header className="App-header">
          <DisplayPokemon pokemon={pokemon}/>
      </header> 
    }
      
    </div>
  );
}

export default App;
